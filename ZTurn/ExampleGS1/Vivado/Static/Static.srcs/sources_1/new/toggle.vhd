----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity toggle is
  generic (nr_of_cycles : integer := 50000000); -- number of (clock) cycles after the output should toggle
  port ( 
    clock : in std_logic;
    reset : in std_logic;
    toggle : out std_logic
    );
end toggle;

architecture behaviour of toggle is
  signal cnt: signed(31 downto 0);
  signal i_toggle : std_logic;
begin

  process(clock, reset)
  begin    
    if (reset = '1') then
      cnt <= (others => '0');
      i_toggle <= '0';
    elsif rising_edge(clock) then
      if (cnt >= (nr_of_cycles-1)) then
        cnt <= (others => '0');
        i_toggle <= not i_toggle;
      else
        cnt <= cnt + 1;
        i_toggle <= i_toggle;            
      end if;
    end if;
  end process;
    
  toggle <= i_toggle;    

end behaviour;
