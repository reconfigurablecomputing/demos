----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top_tb is
--  Port ( );
end top_tb;

architecture behaviour of top_tb is
  -- component section
  component top is
    port ( clock : in std_logic;
           reset : in std_logic;
           sw : in std_logic_vector(3 downto 0);
           led : out std_logic_vector(4 downto 0));
  end component;

  -- signal section
  signal i_reset : std_logic;
  signal i_clk : std_logic;
  signal i_data_in : std_logic_vector(3 downto 0); 
  signal i_data_out : std_logic_vector(4 downto 0);
  signal i_cnt : unsigned(31 downto 0);
begin

  -- instantiate top
  inst_top: top port map (clock => i_clk, reset => i_reset, sw => i_data_in, led => i_data_out);

  -- reset generation
  p_reset: process
  begin
    i_reset <= '1';
    wait for 8 ns;
    i_reset <= '0';
    wait; -- wait forever after first reset
  end process;  

  -- clock generation 4+4ns=125MHz
  p_clock: process
  begin
    i_clk <= '0';
    wait for 4 ns;
    i_clk <= '1';
    wait for 4 ns;
  end process;
  
  -- data generation
  p_data: process
  begin
    i_data_in <= "0000";
    wait for 8 ns;
    i_data_in <= "0001";
    wait for 8 ns;
    i_data_in <= "0010";
    wait for 8 ns;
    i_data_in <= "0011";
    wait;        
  end process;

  
  -- stop the simulation after a certain amount of clock cycles
  p_sim_end: process (i_reset, i_clk)
   begin
    if (i_reset = '1') then
      i_cnt <= (others => '0');
    elsif rising_edge(i_clk) then
       i_cnt <= i_cnt + 1;
    end if;
    
    if i_cnt >= 125 then
      assert FALSE Report "Simulation Finished" severity FAILURE;
    end if;
  end process;

end behaviour;
