----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity adder is
  generic (
      clock_freq : integer := 125000000        -- clock frequency in Hz
  );  
  port(
    clock: in std_logic;
    reset: in std_logic; 
    data_in: in unsigned(3 downto 0);
    data_out: out unsigned(3 downto 0));
end adder;

architecture behaviour of adder is
  -- component section

  -- signal section
  signal sig_data: unsigned(3 downto 0);
  signal sum:  unsigned(3 downto 0);
begin

  -- adder counter logic
  adder: process (reset, clock)
  begin    
    if (reset = '1') then
      sum <= (others => '0');
    elsif rising_edge(clock) then
     -- 2 bit adder
      sum <= ("00" & data_in(3 downto 2)) +  data_in(1 downto 0);
    end if;
  end process;

  data_out <= sum;

end behaviour;

