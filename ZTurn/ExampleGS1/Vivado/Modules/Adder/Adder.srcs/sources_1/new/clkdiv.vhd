----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity clkdiv is
  generic (
    clock_freq : integer := 125000000         -- clock frequency in Hz
        
  );
  port(
    clock: in std_logic;
    reset: in std_logic; 
    clock_out: out std_logic);
end clkdiv;

-- clock divider 1 Hz, cnt should be 62500000 for a 125MHz input clock
architecture behaviour of clkdiv is
  constant c_freq_desired : integer := 1;
  constant c_freq : integer := clock_freq / (2 * c_freq_desired);
  signal cnt: signed(31 downto 0);
  signal clk: std_logic;
begin

  process(clock, reset)
  begin    
    if (reset = '1') then
      cnt <= (others => '0');
      clk <= '0';
    elsif rising_edge(clock) then
      if (cnt >= c_freq) then
        cnt <= (others => '0');
        clk <= not clk;
      else
        cnt <= cnt + 1;
        clk <= clk;            
      end if;
    end if;
  end process;
    
  clock_out <= clk;  

end behaviour;
