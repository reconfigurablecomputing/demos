----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Jeroen ter Haar
-- 
-- Create Date: 06/24/2020 12:46:15 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
  port ( 
    clock : in std_logic;
    -- reset : in std_logic;
    sw : in std_logic_vector(3 downto 0);
    led : out std_logic_vector(2 downto 0)
    );
end top;

architecture Behavioral of top is
  signal cnt: signed(31 downto 0);
  signal ledcnt: signed(1 downto 0);
  signal clock1Hz : std_logic;
  signal reset : std_logic;
begin

  reset <= sw(0);

  process(clock, reset)
  begin    
    if (reset = '1') then
      cnt <= (others => '0');
      clock1Hz <= '0';
    elsif rising_edge(clock) then
      if (cnt >= (12000000)) then
        cnt <= (others => '0');
        clock1Hz <= not clock1Hz;
      else
        cnt <= cnt + 1;
        clock1Hz <= clock1Hz;            
      end if;
    end if;
  end process;
  
  process(clock1Hz, reset)
  begin    
    if (reset = '1') then
      ledcnt <= (others => '1');
    elsif rising_edge(clock1Hz) then
      if (ledcnt > 2) then
        ledcnt <= (others => '0');        
      else
        ledcnt <= ledcnt + 1;                    
      end if;
    end if;
  end process;  
  
  led <= "110" when ledcnt = "00" else
         "101" when ledcnt = "01" else
         "011" when ledcnt = "10" else
         "111" when ledcnt = "11";

end Behavioral;
