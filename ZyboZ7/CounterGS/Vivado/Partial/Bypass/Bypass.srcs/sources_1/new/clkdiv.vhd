----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- clock divider 1 Hz for cnt 62500000 and 125MHz input clock
entity clkdiv is
  port(
    clock: in std_logic;
    reset: in std_logic; 
    clock_out: out std_logic);
end clkdiv;

architecture behaviour of clkdiv is
  signal cnt: signed(31 downto 0);
  signal clk: std_logic;
begin

  process(clock, reset)
  begin    
    if (reset = '1') then
      cnt <= (others => '0');
      clk <= '0';
    elsif rising_edge(clock) then
      if (cnt >= 62500000) then
        cnt <= (others => '0');
        clk <= not clk;
      else
        cnt <= cnt + 1;
        clk <= clk;            
      end if;
    end if;
  end process;
    
  clock_out <= clk;  

end behaviour;
