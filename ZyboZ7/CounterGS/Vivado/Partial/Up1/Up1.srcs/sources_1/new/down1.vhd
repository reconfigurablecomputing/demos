----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity down1 is
  port(
    clock: in std_logic;
    reset: in std_logic; 
    data_in: in unsigned(3 downto 0);
    data_out: out unsigned(3 downto 0));
end down1;

architecture behaviour of down1 is

  component clkdiv is
    port(
      clock: in std_logic;
      reset: in std_logic; 
      clock_out: out std_logic);
  end component;

  signal sig_data: unsigned(3 downto 0);
  signal clk_div: std_logic;
begin


  -- instantiate the clock divider
  inst_clkdiv: clkdiv port map (clock, reset, clk_div);

  -- down1 counter logic
  down1: process (clock, reset)
  begin    
    if (reset = '1') then
      sig_data <= (others => '0');
    elsif rising_edge(clock) then
      if (clk_div = '1') then
        sig_data <= data_in - 1;
      else
        sig_data <= sig_data;        
      end if;
    end if;
  end process;

  data_out <= sig_data;

end behaviour;

