----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity up1 is
  generic (
      clock_freq : integer := 125000000        -- clock frequency in Hz
  );  
  port(
    clock: in std_logic;
    reset: in std_logic; 
    data_in: in unsigned(3 downto 0);
    data_out: out unsigned(3 downto 0));
end up1;

architecture behaviour of up1 is

  component clkdiv is
    generic (
      clock_freq : integer := 125000000        -- clock frequency in Hz
    );  
    port(
      clock: in std_logic;
      reset: in std_logic; 
      clock_out: out std_logic);
  end component;

  signal sig_data: unsigned(3 downto 0);
  signal clk_div: std_logic;
begin

  -- instantiate the clock divider
  inst_clkdiv: clkdiv 
    generic map(clock_freq => clock_freq)
    port map (clock => clock, reset => reset, clock_out => clk_div);

  -- up1 counter logic
  up1: process (reset, clk_div)
  begin    
    if (reset = '1') then
      sig_data <= (others => '0');
    elsif rising_edge(clk_div) then
    --  if (rising_edge(clk_div)) then
        sig_data <= sig_data + data_in;
    --  else
    --    sig_data <= sig_data;        
    --  end if;
    end if;
  end process;

  data_out <= sig_data;

end behaviour;

