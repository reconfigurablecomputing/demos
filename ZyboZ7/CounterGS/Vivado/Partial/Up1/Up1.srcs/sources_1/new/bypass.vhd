----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bypass is
  port(
    clock: in std_logic;
    reset: in std_logic; 
    data_in: in unsigned(3 downto 0);
    data_out: out unsigned(3 downto 0));
end bypass;

architecture behaviour of bypass is
begin

  data_out <= data_in;

end behaviour;

