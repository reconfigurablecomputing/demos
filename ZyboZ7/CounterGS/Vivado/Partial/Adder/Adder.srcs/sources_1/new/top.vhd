----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is
  port ( 
    clock : in std_logic);
end top;

-- defines the top entity of the module
architecture behaviour of top is

  component adder is
    generic (
      clock_freq : integer
    );    
    port(
      clock: in std_logic;
      reset: in std_logic; 
      data_in: in unsigned(3 downto 0);
      data_out: out unsigned(3 downto 0));
  end component;

  signal reset: std_logic; 
  signal data_in: unsigned(3 downto 0);
  signal data_out: unsigned(3 downto 0);

begin  

inst_adder : adder
  generic map (clock_freq => 125000000)
  port map (clock => clock, reset => reset, data_in => data_in, data_out => data_out );

end behaviour;