----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;                                                                          
                                               
library UNISIM;                                                                                           
use UNISIM.VComponents.all;  

entity top is
  generic (
    clock_freq : integer := 125000000;         -- clock frequency in Hz
    clock_cycle_delay : integer := 50000000    -- define after how many clock cycles the output should toggle    
  );
  port ( clock : in std_logic;
         reset : in std_logic;
         sw : in std_logic_vector(3 downto 0);
         led : out std_logic_vector(4 downto 0));
end top;

architecture behaviour of top is

-- connection macro
--component ConnMacro is port (
--	p2s : out std_logic_vector(3 downto 0);
--	s2p : in std_logic_vector(4 downto 0));
--end component;

component toggle is 
generic (clock_cycles_delay : integer);
port (
  clock : in std_logic;
  reset : in std_logic;
  toggle : out std_logic);
end component;

begin

--  inst_ConnMacro : ConnMacro
--  port map ( 
--    p2s => led(3 downto 0),
--    s2p(4) => reset,
--    s2p(3 downto 0) => sw
--  );

  led(3 downto 0) <= (others=>'0');

  -- this instance blinks led(4), LD5
  inst_tm : toggle
  generic map (clock_cycles_delay => clock_cycle_delay) 
  port map (clock => clock, reset => reset, toggle => led(4));

end behaviour;
