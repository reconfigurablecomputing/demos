#-----------------------------------------------------------------------------------------
# Project name:   DPR Counter
# Engineer:       Jeroen ter Haar
# Company:        University of Twente
# Create date:    05-12-2019
# Description:    Static System
#                 In an attempt to get it working for the ZyboZ7 board
#                 We use the same binFGPA as on the ZedBoard.
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# Global variables
#-----------------------------------------------------------------------------------------
Set Variable=Device Value=%GOAHEAD_HOME%/Devices/xc7z020clg484.binFPGA; 
Set Variable=VivadoProjectTop  Value=C:/git/Demos/ZyboZ7/Counter/Vivado/STC/STC;  
Set Variable=VivadoProjectSrcs Value=C:/git/Demos/ZyboZ7/Counter/Vivado/STC/STC/STC.srcs/sources_1/new;

#-----------------------------------------------------------------------------------------
# Load device
#-----------------------------------------------------------------------------------------
OpenBinFPGA FileName=%Device%;

#-----------------------------------------------------------------------------------------
# Floorplan the partial area  
#-----------------------------------------------------------------------------------------
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y49 LowerRightTile=INT_R_X49Y0;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=PartialArea;
ExpandSelectionSpecialTiles;
StoreCurrentSelectionAs UserSelectionType=PartialArea_All;

#-----------------------------------------------------------------------------------------
# Connect clock pins in the partial area
#
# Connect all the clock pins of all currently selected tiles to the given clock net
#-----------------------------------------------------------------------------------------
ConnectClockPins ClockPin=C BELs=[A-D]FF ClockNetName=clock_IBUF_BUFG FileName=%VivadoProjectTop%/static_connect_clockpins.tcl Append=False CreateBackupFile=False;

#-----------------------------------------------------------------------------------------
# Connection macro (CM) placement
#
# The connection macro is a placeholder. The connection macro is placed within the partial 
# area. The connection macro are LUTs, where the interface signals of the partial modules 
# are connected to.  
#-----------------------------------------------------------------------------------------
ClearSelection;
#Why do we get a vector of of size 4? With a single CLB we can have at least 8 inputs and outputs?
AddBlockToSelection UpperLeftTile=INT_L_X40Y17 LowerRightTile=INT_L_X40Y17;
#What happens if we include tile INT_L_X40Y16 (see line below)? => we get 8 instances of lut and p2s,s2p are 8 bit wide
# AddBlockToSelection UpperLeftTile=INT_L_X40Y16 LowerRightTile=INT_L_X40Y17;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnMacroPlace;

#-----------------------------------------------------------------------------------------
# Generate VHDL wrapper and location constraints for the connection macro
#-----------------------------------------------------------------------------------------
AddInstantiationInSelectedTiles Mode=row-wise Horizontal=left-to-right Vertical=top-down SliceNumber=1 Filter=.* InstanceName=inst Hierarchy= LibraryElementName=VivadoConnectionPrimitive AutoClearModuleSlot=False NetlistContainerName=default_netlist_container;

#Specify the interface signals of the connection macro. 
AnnotateSignalNames InstantiationFilter=.* LibraryElementFilter=.* StartIndex=0 StepWidth=1 PortMapping=I(0|1|2|3):s2p:external:0-1-3-2,I(4|5):0:external,O:p2s:external;
#Why do we have 0-1-3-2? => It has something to do with the possible/optimal routing to the LUTs on the fabric itself.

#Generate VHDL and constraint files
PrintVHDLWrapper InstantiationFilter=.* EntityName=ConnMacro FileName=%VivadoProjectSrcs%/ConnMacro.vhd Append=False CreateBackupFile=False;
PrintVHDLWrapperInstantiation InstantiationFilter=:* EntityName=ConnMacro FileName=%VivadoProjectTop%/ConnMacro_inst.vhd Append=False CreateBackupFile=False;
PrintLocationConstraints InstantiationFilter=.* HierarchyPrefix=inst_ConnMacro/ FileName=%VivadoProjectTop%/static_location_constraints.tcl Append=False CreateBackupFile=False;

#-----------------------------------------------------------------------------------------
# Create tunnel inside the partial area 
#
# We create a tunnel, which forces the router to use one specific path to route from the 
# static design to the reconfigurable region. 
#-----------------------------------------------------------------------------------------
ClearSelection;
SelectUserSelection UserSelectionType=ConnMacroPlace;
ExpandSelectionLeftAndRight UserSelectionType=PartialArea_All;
DoNotBlockDoubleEast;

#-----------------------------------------------------------------------------------------
# Generate the constraints to prohibit the placer to allocate any logic resources inside
# the partial area
#-----------------------------------------------------------------------------------------
ClearSelection;
SelectUserSelection UserSelectionType=PartialArea;
PrintProhibitStatementsForSelection ExcludeUsedSlices=False FileName=%VivadoProjectTop%/static_prohibits.tcl Append=False CreateBackupFile=False;

#-----------------------------------------------------------------------------------------
# Generate the blocker that prevents the router to use any wires inside the 
# partial area, except for the tunnel wires
#-----------------------------------------------------------------------------------------
BlockSelection PrintUnblockedPorts=True UnblockedPortsToIgnore=(CTRL_)|(GND_WIRE)|(LOGIC)|(BYP_BOUNCE1) Prefix=blocker_net BlockWithEndPips=True SliceNumber=0 NetlistContainerName=default_netlist_container;
SaveAsBlocker NetlistContainerNames= FileName=%VivadoProjectTop%/static_blocker.tcl;
