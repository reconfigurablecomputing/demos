library IEEE;                                                                                               
use IEEE.STD_LOGIC_1164.ALL;   
use IEEE.STD_LOGIC_ARITH.ALL;                                                                             
use IEEE.STD_LOGIC_UNSIGNED.ALL;                                                                            
                                               
library UNISIM;                                                                                           
use UNISIM.VComponents.all;                                                                          
 
entity mdl is                                                                                        
  port ( clock : in std_logic;
         reset_in : in std_logic;
         reset_out : out std_logic;
         count_in : in unsigned(2 downto 0); 
		 count_out : out unsigned(2 downto 0));
end mdl;                                                                                             

architecture Behavioral of mdl is                                                                     

signal i_count : unsigned(2 downto 0);

begin  

  process(clock, reset_in)
  begin
    if rising_edge(clock) then
      if reset_in='1' then
        i_count <= (others => '0');
      else
        i_count <= count_in + 1;
      end if;
    end if;
  end process;

  count_out <= i_count;
  
  reset_out <= reset_in;
  
end Behavioral;                                                                                             