-- This file was automatically generated by GoAhead

-- Normally, there is no need for modifying this file.
-- However, there are comments with the keywords "architecture_declaration" 
-- and "architecture_body" that easily allow adding extra code to this file
-- Example for adding "internal_signal_a <= internal_signal_b" to the body of your_file.vhd:
-- cat your_file.vhd | sed 's/-- architecture_body/internal_signal_a := internal_signal_b;/g' | tr ":" "<" > out.vhd
-- Note that we use ":=" instead of "<=", which is corrected by tr (translate)

----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ConnMacro is port (
	p2s : out std_logic_vector(3 downto 0);
	s2p : in std_logic_vector(3 downto 0));

end ConnMacro;

architecture Behavioral of ConnMacro is

-- architecture_declaration

attribute s : string;
attribute keep : string;
attribute MARK_DEBUG : string;

-- attribute assignments

attribute MARK_DEBUG of p2s : signal is "TRUE";
attribute MARK_DEBUG of s2p : signal is "TRUE";

begin

-- architecture_body

-- instantiation of inst_0
inst_0 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => p2s(0),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_1
inst_1 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => p2s(1),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_2
inst_2 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => p2s(2),
	I0 => s2p(0),
	I1 => s2p(1),
	I2 => s2p(3),
	I3 => s2p(2),
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_3
inst_3 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => p2s(3),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

end architecture Behavioral;
