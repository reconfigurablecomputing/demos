----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.04.2019 13:43:07
-- Design Name: 
-- Module Name: ToggleModule - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ToggleModule is
    Port ( clock : in STD_LOGIC;
           reset : in STD_LOGIC;
           toggle : out STD_LOGIC);
end ToggleModule;

architecture Behavioral of ToggleModule is

signal i_toggle : std_logic;

begin

  process(clock, reset)
  begin
    if rising_edge(clock) then
      if (reset = '1') then
        i_toggle <= '0';
      else 
        i_toggle <= not i_toggle;
      end if;
    end if;
  end process;

  toggle <= i_toggle;

end Behavioral;
