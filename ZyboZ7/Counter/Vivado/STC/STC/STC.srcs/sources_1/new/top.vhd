----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.03.2019 14:09:40
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;                                                                                               
use IEEE.STD_LOGIC_1164.ALL;   
use IEEE.STD_LOGIC_ARITH.ALL;                                                                             
use IEEE.STD_LOGIC_UNSIGNED.ALL;                                                                            
                                               
library UNISIM;                                                                                           
use UNISIM.VComponents.all;  

entity top is
    Port ( clock : in STD_LOGIC;
           reset : in STD_LOGIC;
           sw : in std_logic_vector(2 downto 0);
           led : out STD_LOGIC_VECTOR(4 downto 0));
end top;

architecture Behavioral of top is

-- connection macro
component ConnMacro is port (
	p2s : out std_logic_vector(3 downto 0);
	s2p : in std_logic_vector(3 downto 0));
end component ConnMacro;

component ToggleModule is port (
    clock : in STD_LOGIC;
    reset : in STD_LOGIC;
    toggle : out STD_LOGIC);
end component ToggleModule;

begin

inst_ConnMacro : ConnMacro
port map ( 
  p2s => led(3 downto 0),
  s2p(3) => reset,
  s2p(2 downto 0) => sw
);

inst_ToggleModule : ToggleModule
port map (
  clock => clock,
  reset => reset, 
  toggle => led(4)
);

end Behavioral;
