place_cell inst_ConnMacro/inst_0 SLICE_X61Y17/A6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_ConnMacro/inst_0]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_ConnMacro/inst_0]; # generated_by_GoAhead

place_cell inst_ConnMacro/inst_1 SLICE_X61Y17/B6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_ConnMacro/inst_1]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_ConnMacro/inst_1]; # generated_by_GoAhead

place_cell inst_ConnMacro/inst_2 SLICE_X61Y17/C6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_ConnMacro/inst_2]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_ConnMacro/inst_2]; # generated_by_GoAhead

place_cell inst_ConnMacro/inst_3 SLICE_X61Y17/D6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_ConnMacro/inst_3]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_ConnMacro/inst_3]; # generated_by_GoAhead

