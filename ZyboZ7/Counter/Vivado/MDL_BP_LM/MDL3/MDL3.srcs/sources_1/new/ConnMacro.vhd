-- This file was automatically generated by GoAhead

-- Normally, there is no need for modifying this file.
-- However, there are comments with the keywords "architecture_declaration" 
-- and "architecture_body" that easily allow adding extra code to this file
-- Example for adding "internal_signal_a <= internal_signal_b" to the body of your_file.vhd:
-- cat your_file.vhd | sed 's/-- architecture_body/internal_signal_a := internal_signal_b;/g' | tr ":" "<" > out.vhd
-- Note that we use ":=" instead of "<=", which is corrected by tr (translate)

----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity MacroConnLeft is port (
	s2p : out std_logic_vector(3 downto 0));

end MacroConnLeft;

architecture Behavioral of MacroConnLeft is

-- architecture_declaration

attribute s : string;
attribute keep : string;

begin

-- architecture_body

-- instantiation of inst_L_0
inst_L_0 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => s2p(0),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_L_1
inst_L_1 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => s2p(1),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_L_2
inst_L_2 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => s2p(2),
	I0 => '1',
	I1 => '1',
	I2 => '1',
	I3 => '1',
	I4 => '1',
	I5 => '1'
);

-- instantiation of inst_L_3
inst_L_3 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => s2p(3),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

end architecture Behavioral;
-- This file was automatically generated by GoAhead

-- Normally, there is no need for modifying this file.
-- However, there are comments with the keywords "architecture_declaration" 
-- and "architecture_body" that easily allow adding extra code to this file
-- Example for adding "internal_signal_a <= internal_signal_b" to the body of your_file.vhd:
-- cat your_file.vhd | sed 's/-- architecture_body/internal_signal_a := internal_signal_b;/g' | tr ":" "<" > out.vhd
-- Note that we use ":=" instead of "<=", which is corrected by tr (translate)

----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity MacroConnRight is port (
	p2s : in std_logic_vector(3 downto 0));

end MacroConnRight;

architecture Behavioral of MacroConnRight is

-- architecture_declaration

attribute s : string;
attribute keep : string;

signal dummy : std_logic_vector(3 downto 0) := (others => '1');
attribute s of dummy : signal is "true";
attribute keep of dummy : signal is "true";

begin

-- architecture_body

-- instantiation of inst_R_0
inst_R_0 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(0),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_R_1
inst_R_1 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(1),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_R_2
inst_R_2 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(2),
	I0 => p2s(0),
	I1 => p2s(1),
	I2 => p2s(3),
	I3 => p2s(2),
	I4 => '1',
	I5 => '1'
);

-- instantiation of inst_R_3
inst_R_3 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(3),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

end architecture Behavioral;
-- This file was automatically generated by GoAhead

-- Normally, there is no need for modifying this file.
-- However, there are comments with the keywords "architecture_declaration" 
-- and "architecture_body" that easily allow adding extra code to this file
-- Example for adding "internal_signal_a <= internal_signal_b" to the body of your_file.vhd:
-- cat your_file.vhd | sed 's/-- architecture_body/internal_signal_a := internal_signal_b;/g' | tr ":" "<" > out.vhd
-- Note that we use ":=" instead of "<=", which is corrected by tr (translate)

----------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity MacroConnRight is port (
	p2s : in std_logic_vector(3 downto 0));

end MacroConnRight;

architecture Behavioral of MacroConnRight is

-- architecture_declaration

attribute s : string;
attribute keep : string;

signal dummy : std_logic_vector(3 downto 0) := (others => '1');
attribute s of dummy : signal is "true";
attribute keep of dummy : signal is "true";

begin

-- architecture_body

-- instantiation of inst_R_0
inst_R_0 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(0),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_R_1
inst_R_1 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(1),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

-- instantiation of inst_R_2
inst_R_2 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(2),
	I0 => p2s(0),
	I1 => p2s(1),
	I2 => p2s(3),
	I3 => p2s(2),
	I4 => '1',
	I5 => '1'
);

-- instantiation of inst_R_3
inst_R_3 : LUT6
generic map ( INIT => X"ABCDABCDABCDABCD" )
port map (
	O => dummy(3),
	I0 => '0',
	I1 => '0',
	I2 => '0',
	I3 => '0',
	I4 => '0',
	I5 => '0'
);

end architecture Behavioral;
