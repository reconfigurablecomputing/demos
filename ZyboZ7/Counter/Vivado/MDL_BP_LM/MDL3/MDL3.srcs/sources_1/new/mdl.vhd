library IEEE;                                                                                               
use IEEE.STD_LOGIC_1164.ALL;   
use IEEE.STD_LOGIC_ARITH.ALL;                                                                             
use IEEE.STD_LOGIC_UNSIGNED.ALL;                                                                            
                                               
library UNISIM;                                                                                           
use UNISIM.VComponents.all;                                                                          
 
entity mdl is                                                                                        
  port ( clock : in std_logic;
         reset_in : in std_logic;
         reset_out : out std_logic;
         count_in : in unsigned(2 downto 0); 
		 count_out : out unsigned(2 downto 0));
end mdl;                                                                                             

architecture Behavioral of mdl is                                                                     

begin  

  count_out <= count_in;
  
  reset_out <= reset_in;
  
end Behavioral;                                                                                             