place_cell inst_MacroConnLeft/inst_L_0 SLICE_X51Y17/A6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnLeft/inst_L_0]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnLeft/inst_L_0]; # generated_by_GoAhead

place_cell inst_MacroConnLeft/inst_L_1 SLICE_X51Y17/B6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnLeft/inst_L_1]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnLeft/inst_L_1]; # generated_by_GoAhead

place_cell inst_MacroConnLeft/inst_L_2 SLICE_X51Y17/C6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnLeft/inst_L_2]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnLeft/inst_L_2]; # generated_by_GoAhead

place_cell inst_MacroConnLeft/inst_L_3 SLICE_X51Y17/D6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnLeft/inst_L_3]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnLeft/inst_L_3]; # generated_by_GoAhead

place_cell inst_MacroConnRight/inst_R_0 SLICE_X73Y17/A6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnRight/inst_R_0]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnRight/inst_R_0]; # generated_by_GoAhead

place_cell inst_MacroConnRight/inst_R_1 SLICE_X73Y17/B6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnRight/inst_R_1]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnRight/inst_R_1]; # generated_by_GoAhead

place_cell inst_MacroConnRight/inst_R_2 SLICE_X73Y17/C6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnRight/inst_R_2]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnRight/inst_R_2]; # generated_by_GoAhead

place_cell inst_MacroConnRight/inst_R_3 SLICE_X73Y17/D6LUT; # generated_by_GoAhead
set_property LOCK_PINS {I0:A1 I1:A2 I2:A3 I3:A4 I4:A5 I5:A6} [get_cells inst_MacroConnRight/inst_R_3]; # generated_by_GoAhead
set_property DONT_TOUCH TRUE [get_cells inst_MacroConnRight/inst_R_3]; # generated_by_GoAhead

