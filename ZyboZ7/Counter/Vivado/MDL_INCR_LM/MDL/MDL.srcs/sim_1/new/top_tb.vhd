----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.03.2019 10:07:05
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;                                                                                               
use IEEE.STD_LOGIC_1164.ALL;   
use IEEE.STD_LOGIC_ARITH.ALL;                                                                             
use IEEE.STD_LOGIC_UNSIGNED.ALL;                                                                            
                                               
library UNISIM;                                                                                           
use UNISIM.VComponents.all;  

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is
component mdl
  port ( clock : in std_logic;
         reset : in std_logic;
         count_in : in unsigned(2 downto 0); 
		 count_out : out unsigned(3 downto 0));
end component;
  
  -- internal signals
  signal i_reset : std_logic;
  signal i_clk : std_logic;
  signal i_count_in : unsigned(2 downto 0); 
  signal i_count_out : unsigned(3 downto 0);
  
begin

  UUT: mdl port map 
  (
    reset => i_reset,
    clock => i_clk,
    count_in => i_count_in,
    count_out => i_count_out
  );
  
  -- reset generation
  process
  begin
    i_reset <= '1';
    wait for 20 ns;
    i_reset <= '0';
    wait for 1000 ns;
  end process;
  
  -- clock generation
  process
  begin
    i_clk <= '0';
    wait for 10 ns;
    i_clk <= '1';
    wait for 10 ns;
  end process;
  
  -- count_in generation
  process
  begin
    i_count_in <= "000";
    wait for 50 ns;
    i_count_in <= "001";
    wait for 50 ns;
    i_count_in <= "010";
    wait for 50 ns;
    i_count_in <= "011";
    wait for 50 ns;
    i_count_in <= "100";
    wait for 50 ns;
    i_count_in <= "101";
    wait for 50 ns;
    i_count_in <= "110";
    wait for 50 ns;
    i_count_in <= "111";
    wait for 50 ns;
    i_count_in <= "000";
    wait for 50 ns;
    i_count_in <= "111";
    wait for 50 ns;
    
  end process;


end Behavioral;
