----------------------------------------------------------------------------------
-- Company: University of Twente
-- Engineer: Tom Hogenkamp 
-- 
-- Create Date: 05.04.2019 10:42:55
-- Design Name:  
-- Module Name: top - Behavioral
-- Project Name: Simple PR Application
-- Target Devices: Zynq
-- Tool Versions: 2018.2
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;                                                                                               
use IEEE.STD_LOGIC_1164.ALL;   
use IEEE.STD_LOGIC_ARITH.ALL;                                                                             
use IEEE.STD_LOGIC_UNSIGNED.ALL;                                                                            
                                               
library UNISIM;                                                                                           
use UNISIM.VComponents.all;  

entity top is
    port ( clock : in STD_LOGIC);
end top;

architecture Behavioral of top is

-- attribute_declaration
attribute MARK_DEBUG : string;
attribute keep : string;

-- component_declaration
component mdl
  port ( clock : in std_logic;
         reset_in : in std_logic;
         reset_out : out std_logic;
         count_in : in unsigned(2 downto 0); 
         count_out : out unsigned(2 downto 0));
end component;

component MacroConnLeft 
port (
	s2p : out std_logic_vector(3 downto 0));
end component;

component MacroConnRight 
port (
	p2s : in std_logic_vector(3 downto 0));
end component;

-- signal declaration
signal macro_to_mdl, mdl_to_macro : std_logic_vector(3 downto 0);

-- attribute_assignment
attribute MARK_DEBUG of macro_to_mdl : signal is "TRUE";
attribute keep of macro_to_mdl : signal is "true";
attribute MARK_DEBUG of mdl_to_macro : signal is "TRUE";
attribute keep of mdl_to_macro : signal is "true";

begin

inst_MacroConnLeft : MacroConnLeft
port map (
  s2p => macro_to_mdl
);

inst_MacroConnRight : MacroConnRight
port map (
  p2s => mdl_to_macro
);

inst_Module : mdl
port map (
  clock => clock,
  reset_in => macro_to_mdl(3),
  count_in => unsigned(macro_to_mdl(2 downto 0)),
  reset_out => mdl_to_macro(3),
  std_logic_vector(count_out) => mdl_to_macro(2 downto 0)
);

end Behavioral;
