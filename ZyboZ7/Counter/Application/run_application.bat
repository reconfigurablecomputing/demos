@ECHO OFF
CALL configuration.bat

SET VivadoProjects=C:/UT/ModuleRelocationDemo/Counter-Zybo/Vivado
SET bitfile=%VivadoProjects%/STC/STC/STC_post_route_without_blocker_NoCRC.bit
SET module=INC
SET resource_type=LM
SET slot=20

REM The static system contains the reconfigurable region. Partial bitstream are later generated to load modules to this reconfigurable region.
REM The static region is located in the first clock row and is 12 tiles wide (from slot 19 to 24). See ZedBoard_partial_slots.
REM If our modules are two tiles wide, we can load a maximum of 6 modules simultaneously. 
REM In the following, we program the static bitstream 
ECHO Load the static bitstream..
CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet
PAUSE 

REM Currently, the bitstream containing the module is a full bitstream. We use BitMan to cut out the module area and generate a partial bitstream from this area. 
REM The slot size is 2 tiles in width and 1 clock region in height. The resource type is CLB_L+CLB_M. Thus, we can only relocate the module in CLB_L+CLB_M slots!
REM The bitmap.exe takes the following parameters:
REM bitman.exe  -x  left_bottom_X  left_bottom_Y  right_top_X  right_top_Y  full_bitstream  new_left_bottom_X  new_left_bottom_Y  partial_bitstream  
CALL .\BitMan\bitman.exe -x 40 0 41 49 %VivadoProjects%/MDL_INCR_LM/MDL/MDL_post_route_without_blocker_NoCRC.bit -M 40 0 partial_bitstream_%module%_%resource_type%_%slot%.bit
SET bitfile=partial_bitstream_%module%_%resource_type%_%slot%.bit

REM program the partial bitstream
ECHO Load the partial bitstream.. (INC, footprint CLB_L+CLB_M, slot 20)
CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet
REM The output on the LEDs should be Output=Input+1

ECHO Press enter to implement another module 
PAUSE

SET module=INC
SET resource_type=LM
SET slot=19

REM Now, we use the same module, but configure it in slot 19. Module relocation! 
CALL .\BitMan\bitman.exe -x 40 0 41 49 %VivadoProjects%/MDL_INCR_LM/MDL/MDL_post_route_without_blocker_NoCRC.bit -M 38 0 partial_bitstream_%module%_%resource_type%_%slot%.bit
set bitfile=partial_bitstream_%module%_%resource_type%_%slot%.bit

REM program the partial bitstream
ECHO Load the partial bitstream.. (INC, footprint CLB_L+CLB_M, slot 19)
CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

REM The output on the LEDs should be Output=Input+2

ECHO Press enter to implement another module 
PAUSE 

SET module=INC
SET resource_type=LL
SET slot=22

REM Here we create another partial bitstream. 
REM This time we use another module. The functionality is the same (both increments the incoming signal by one), however the resource type is CLB_L+CLB_L. Thus, this module can only be relocated in CLB_L+CLB_L slots!
REM The module size is 2 tiles width and 1 clock region in height. The location of the module in the full bitstream is in slot 22 (see ZedBoard_partial_slots.txt). We cut out the module from this location and relocate it to slot 23.
CALL .\BitMan\bitman.exe -x 44 0 45 49 %VivadoProjects%/MDL_INCR_LL/MDL2/MDL_post_route_without_blocker_NoCRC.bit -M 46 0 partial_bitstream_%module%_%resource_type%_%slot%.bit
set bitfile=partial_bitstream_%module%_%resource_type%_%slot%.bit

REM Program the partial bitstream
ECHO Load the partial bitstream.. (INC, footprint CLB_L+CLB_L, slot 23)
CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

REM The output on the LEDs should be Output=Input+3

ECHO Press enter to implement another module
PAUSE 

SET resource_type=LL
SET module=INC
SET slot=24

REM Configure the same module, but now in slot 24.
CALL .\BitMan\bitman.exe -x 44 0 45 49 %VivadoProjects%/MDL_INCR_LL/MDL2/MDL_post_route_without_blocker_NoCRC.bit -M 48 0 partial_bitstream_%module%_%resource_type%_%slot%.bit
set bitfile=partial_bitstream_%module%_%resource_type%_%slot%.bit

REM Program the partial bitstream
ECHO Load the partial bitstream.. (INC, footprint CLB_L+CLB_L, slot 24)
CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

REM The output on the LEDs should be Output=Input+4

ECHO Press enter to implement another module
PAUSE 

SET resource_type=LM
SET module=BP
SET slot=20

REM Now, we create another partial bitstream. 
REM This time we have a module with different functionality. The functionality of the other modules is incrementing the incoming signal by one. This module simply assigns to incoming signal to the outgoing signal: by-pass module. The footprint is CLB_L+CLB_M.
REM The module size is 2 tiles width and 1 clock region in height. The location of the module in the bitstream is in slot 20 (see ZedBoard_partial_slots.txt). Instead of adding a module to an empty slot, we now reconfigure a slot with a module. Therefore, the output should be decremented by one now.
CALL .\BitMan\bitman.exe -x 40 0 41 49 %VivadoProjects%/MDL_BP_LM/MDL3/MDL_post_route_without_blocker_NoCRC.bit -M 40 0 partial_bitstream_%module%_%resource_type%_%slot%.bit
set bitfile=partial_bitstream_%module%_%resource_type%_%slot%.bit

REM Program the partial bitstream
ECHO Load the partial bitstream.. (BYPASS, footprint CLB_L+CLB_M, slot 20)
CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet





