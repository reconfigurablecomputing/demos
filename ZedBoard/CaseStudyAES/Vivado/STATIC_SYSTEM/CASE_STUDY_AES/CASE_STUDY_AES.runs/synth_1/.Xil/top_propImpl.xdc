set_property SRC_FILE_INFO {cfile:D:/CASE_STUDY_AES/CASE_STUDY_AES/CASE_STUDY_AES.srcs/constrs_1/new/top.xdc rfile:../../../CASE_STUDY_AES.srcs/constrs_1/new/top.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y9 [get_ports CLK]
set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F22 [get_ports {BUTTONS[0]}]; #SW0
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G22 [get_ports {BUTTONS[1]}]; #SW1
set_property src_info {type:XDC file:1 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN H22 [get_ports {BUTTONS[2]}]; #SW2
set_property src_info {type:XDC file:1 line:9 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F21 [get_ports {BUTTONS[3]}]; #SW3
