----------------------------------------------------------------------------------
-- COPYRIGHT (c) 2014 ALL RIGHT RESERVED
--
-- COMPANY:					Ruhr-University Bochum, Secure Hardware Group
-- AUTHOR:					Pascal Sasdrich and Jan Richter-Brockmann
--
-- CREATE DATA:			    14/11/2014
-- MODULE NAME:			    TB_AES_Core
--
-- REVISION:				2.00 - Suitable TB for a round-based AES implementation
--
-- LICENCE: 				Please look at licence.txt
-- USAGE INFORMATION:	    Please look at readme.txt. If licence.txt or readme.txt
--							are missing or	if you have questions regarding the code
--							please contact Tim G�neysu (tim.gueneysu@rub.de) and
--							Pascal Sasdrich (pascal.sasdrich@rub.de) and
--                          Jan Richter-Brockmann (jan.richter-brockmann@rub.de)
--
-- THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
-- KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
-- PARTICULAR PURPOSE.
----------------------------------------------------------------------------------



-- IMPORTS
----------------------------------------------------------------------------------
LIBRARY IEEE;
LIBRARY work;
USE IEEE.STD_LOGIC_1164.ALL;



-- ENTITY
----------------------------------------------------------------------------------
ENTITY TB_AES_Core IS
END TB_AES_Core;
 


-- ARCHITECTURE
----------------------------------------------------------------------------------
ARCHITECTURE Behaviour OF TB_AES_Core IS

	-- COMPONENT DECLARATION FOR THE UNIT UNDER TEST ------------------------------
   COMPONENT AES_Core IS
	PORT (CLK 			: IN  STD_LOGIC;
	      RESET         : IN  STD_LOGIC;
		  -- CONTROL PORTS --------------------------------	
          DATA_AVAIL    : IN  STD_LOGIC;
          DATA_READY    : OUT STD_LOGIC;
		  -- DATA PORTS -----------------------------------
          KEY 			: IN  STD_LOGIC_VECTOR (127 DOWNTO 0);
          DATA_IN 	    : IN  STD_LOGIC_VECTOR (127 DOWNTO 0);
          DATA_OUT  	: OUT STD_LOGIC_VECTOR (127 DOWNTO 0));
	END COMPONENT;
        
   -- INPUTS ---------------------------------------------------------------------
   SIGNAL CLK 			: STD_LOGIC := '0';
   SIGNAL RESET 	    : STD_LOGIC := '1';
   SIGNAL DATA_AVAIL    : STD_LOGIC := '0';
   SIGNAL KEY 			: STD_LOGIC_VECTOR (127 DOWNTO 0) := (OTHERS => '0');
   SIGNAL DATA_IN 	    : STD_LOGIC_VECTOR (127 DOWNTO 0) := (OTHERS => '0');

   -- OUTPUTS --------------------------------------------------------------------
   SIGNAL DATA_READY    : STD_LOGIC;
   SIGNAL DATA_OUT 	    : STD_LOGIC_VECTOR (127 DOWNTO 0);

   -- CLOCK PERIOD DEFINITION ----------------------------------------------------
   CONSTANT CLK_PERIOD  : TIME := 10 NS;
 


-- BEHAVIOR
----------------------------------------------------------------------------------
BEGIN

   -- INSTANTIATE THE UNIT UNDER TEST (UUT) --------------------------------------
   UUT: AES_Core
	PORT MAP (
	  CLK 		    => CLK,
	  RESET         => RESET,
      DATA_AVAIL 	=> DATA_AVAIL,
      DATA_READY 	=> DATA_READY,
      KEY 			=> KEY,
      DATA_IN 		=> DATA_IN,
      DATA_OUT  	=> DATA_OUT
	);
	
   -- CLOCK PROCESS DEFINITION ---------------------------------------------------
   CLK_PROCESS : PROCESS
   BEGIN
		CLK <= '1'; WAIT FOR CLK_PERIOD/2;
		CLK <= '0'; WAIT FOR CLK_PERIOD/2;
   END PROCESS;
	
   -- STIMULUS PROCESS -----------------------------------------------------------
   STIM_PROCESS : PROCESS
	BEGIN	
	
      -- HOLD RESET STATE FOR 100 NS.		
      WAIT FOR 100 NS;

      -- INSERT STIMULUS HERE:	
		DATA_IN 		<= X"3243F6A8885A308D313198A2E0370734";
		KEY			    <= X"2B7E151628AED2A6ABF7158809CF4F3C";
						
		WAIT FOR CLK_PERIOD;		
		DATA_AVAIL <= '1';
		RESET <= '0';
		
		WAIT UNTIL DATA_READY = '1';
		
		WAIT FOR CLK_PERIOD;		
		DATA_AVAIL <= '0';
				
		WAIT FOR CLK_PERIOD*10;		
		DATA_AVAIL <= '1';
		
		WAIT UNTIL DATA_READY = '1';
		
		WAIT FOR CLK_PERIOD;
      WAIT;
   END PROCESS;

END;
