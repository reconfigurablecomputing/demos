----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.09.2019 10:15:05
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY top IS
	PORT (CLK 			: IN STD_LOGIC;
	      DATA_AVAIL    : IN STD_LOGIC;
	      DPR_SEL       : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
	      BUTTONS       : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
	      DATA_READY    : OUT STD_LOGIC;
	      LEDS          : OUT STD_LOGIC_VECTOR(3 DOWNTO 0));
END top;

architecture Behavioral of top is
    
    SIGNAL i_CLK 			: STD_LOGIC := '0';
    SIGNAL i_DATA_AVAIL     : STD_LOGIC := '1';
    SIGNAL i_KEY 			: STD_LOGIC_VECTOR (127 DOWNTO 0) := (OTHERS => '0');
    SIGNAL i_DATA_IN 	    : STD_LOGIC_VECTOR (127 DOWNTO 0) := (OTHERS => '0');
    
    SIGNAL i_DATA_READY     : STD_LOGIC;
    SIGNAL i_DATA_OUT       : STD_LOGIC_VECTOR (127 DOWNTO 0);
    
    COMPONENT clk_wiz_0
    PORT (
        clk_out1 : OUT STD_LOGIC;
        clk_in1 : IN STD_LOGIC;
        reset : IN STD_LOGIC
    );
    END COMPONENT;

begin

    -- data can be changed by using the buttons
    i_DATA_IN(127 DOWNTO 4) <= X"3243F6A8885A308D313198A2E037073";
    i_DATA_IN(3 DOWNTO 0) <= BUTTONS;
    
    -- secret key
    i_KEY <= X"2B7E151628AED2A6ABF7158809CF4F3C";
    
    -- data available signal
    i_DATA_AVAIL <= DATA_AVAIL;
    
    -- encrypted data (only the 4 LSB bits)
    LEDS <= i_DATA_OUT(3 DOWNTO 0);
    
    -- data ready signal
    DATA_READY <= i_DATA_READY;
    
    inst_AES: ENTITY work.AES_Core
	PORT MAP (
        CLK 		    => i_CLK,
	    RESET           => '0',
	    DPR_SEL         => DPR_SEL,
        DATA_AVAIL 	    => i_DATA_AVAIL,
        DATA_READY 	    => i_DATA_READY,
        KEY 			=> i_KEY,
        DATA_IN 		=> i_DATA_IN,
        DATA_OUT  	    => i_DATA_OUT
    );
    
    inst_Clock : clk_wiz_0
    PORT MAP (
           clk_out1 => i_CLK,
           clk_in1 => CLK,
           reset => '0' 
        );

end Behavioral;
