# External clock
set_property PACKAGE_PIN Y9 [get_ports CLK]

# Switches
set_property PACKAGE_PIN F22 [get_ports {BUTTONS[0]}];  
set_property PACKAGE_PIN G22 [get_ports {BUTTONS[1]}];   
set_property PACKAGE_PIN H22 [get_ports {BUTTONS[2]}];  
set_property PACKAGE_PIN F21 [get_ports {BUTTONS[3]}];  
set_property PACKAGE_PIN H19 [get_ports {DPR_SEL[0]}]; 
set_property PACKAGE_PIN H18 [get_ports {DPR_SEL[1]}];  
set_property PACKAGE_PIN H17 [get_ports {DPR_SEL[2]}];   
set_property PACKAGE_PIN M15 [get_ports {DATA_AVAIL}];  

# Buttons
set_property PACKAGE_PIN R18 [get_ports {DPR_SEL[3]}]; 
set_property PACKAGE_PIN P16 [get_ports {DPR_SEL[4]}];  
set_property PACKAGE_PIN N15 [get_ports {DPR_SEL[5]}];   

# LEDs
set_property PACKAGE_PIN T22 [get_ports {LEDS[0]}]; 
set_property PACKAGE_PIN T21 [get_ports {LEDS[1]}]; 
set_property PACKAGE_PIN U22 [get_ports {LEDS[2]}]; 
set_property PACKAGE_PIN U21 [get_ports {LEDS[3]}]; 
set_property PACKAGE_PIN U14 [get_ports {DATA_READY}]; 
