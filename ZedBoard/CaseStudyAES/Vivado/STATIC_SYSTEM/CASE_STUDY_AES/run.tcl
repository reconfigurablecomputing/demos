synth_design  -top top 

opt_design -sweep

place_design

route_design  

set_property BITSTREAM.General.UnconstrainedPins {Allow} [current_design]
set_property BITSTREAM.GENERAL.CRC DISABLE [current_design]
write_bitstream -force ./static_bitstream


