# puts $::env(bitfile)
set File $::env(bitfile)
open_hw -quiet
connect_hw_server -quiet
open_hw_target -quiet
current_hw_device [get_hw_devices xc7z020_1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z020_1] 0] -quiet
set_property PROBES.FILE {} [get_hw_devices xc7z020_1]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7z020_1]
set_property PROGRAM.FILE $File [get_hw_devices xc7z020_1]
program_hw_devices [get_hw_devices xc7z020_1] -quiet
refresh_hw_device [lindex [get_hw_devices xc7z020_1] 0] -quiet