@REM ------------------------------------------------------------------------------------
@REM Application description
@REM The application is grid-style, which is divided in 2x2 slots
@REM The static system contains the whole AES algorithm, except the modules within the 
@REM rounds. We first configure the static system. Later on, during run-time, we reconfigure
@REM the partial area and load the modules of the rounds to make the AES algorithm complete
@REM ------------------------------------------------------------------------------------

@REM ------------------------------------------------------------------------------------
@REM Global variables
@REM ------------------------------------------------------------------------------------

@SET topdirectory=D:\CASE_STUDY_AES

@REM ------------------------------------------------------------------------------------

@REM source the Vivado settings: .settings64-Vivado.bat
@CALL D:\Xilinx\Vivado\2018.2\.settings64-Vivado.bat

@REM ------------------------------------------------------------------------------------
@REM Program the static system
@REM 
@REM Footprint: MsLBsMLsMLsM
@REM L: CLBLL
@REM M: CLBLM
@REM B: BlockRAM
@REM s: Two INT tiles
@REM 
@REM Location
@REM Clock region: 0-1
@REM Slots: 17-20 (see ZedBoard_partial_slots.txt)
@REM ------------------------------------------------------------------------------------

@SET bitfile=%topdirectory%\Vivado\STATIC_SYSTEM\CASE_STUDY_AES\static_bitstream.bit
 
@ECHO Load the static bitstream..
@CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

@PAUSE 

@REM ------------------------------------------------------------------------------------
@REM Create partial bitstreams
@REM ------------------------------------------------------------------------------------

@REM create partial bitstream for SubBytes module 
@REM The module is developed on slot (0,0)
@REM Build partial bitstream for slot (0,1)
@CALL .\BitMan\bitman.exe -x 34 50 37 99 %topdirectory%\Vivado\SUBBYTES_MsLBsM\SUBBYTES\partial_bitstream.bit -M 34 0 module_SubBytes.bit

@REM create partial bitstream for ShiftRows module 
@REM The module is developed on slot (1,0)
@REM Build partial bitstream for slot (1,1)
@CALL .\BitMan\bitman.exe -x 38 50 41 99 %topdirectory%\Vivado\SHIFT_ROWS_LsMLsM\SHIFT_ROWS\partial_bitstream.bit -M 38 0 module_ShiftRows.bit

@REM create partial bitstream for MixColumns module 
@REM The module is developed on slot (1,0)
@REM Build partial bitstream for slot (1,0)
@CALL .\BitMan\bitman.exe -x 38 50 41 99 %topdirectory%\Vivado\MIX_COLUMNS_LsMLsM\MIX_COLUMNS\partial_bitstream.bit -M 38 50 module_MixColumns.bit

@PAUSE 

@REM ------------------------------------------------------------------------------------
@REM Program the module
@REM ------------------------------------------------------------------------------------

@ECHO Program the SubBytes module ..

@SET bitfile=%topdirectory%\Applications\module_SubBytes.bit
@CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

@ECHO Program the ShiftRows module ..

@SET bitfile=%topdirectory%\Applications\module_ShiftRows.bit
@CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

@ECHO Program the MixColumns module ..

@SET bitfile=%topdirectory%\Applications\module_MixColumns.bit
@CALL vivado -nolog -nojournal -notrace -mode batch -source load_bitstream.tcl -quiet

@ECHO Finished.



