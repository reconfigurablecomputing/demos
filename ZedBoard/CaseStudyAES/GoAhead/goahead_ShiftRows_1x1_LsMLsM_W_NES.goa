#-----------------------------------------------------------------------------------------
# Project name: SecRec
# Engineer: Tom Hogenkamp
# Company: University of Twente
# Create date: 10-09-2019
# Description: ShiftRow Module, footprint LsMLsM
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# Global variables
#-----------------------------------------------------------------------------------------

Set Variable=ZedBoard Value=%GOAHEAD_HOME%/Devices/xc7z020clg484.binFPGA; 
Set Variable=VivadoProjectTop Value=D:/CASE_STUDY_AES/Vivado/SHIFT_ROWS_LsMLsM/SHIFT_ROWS;  
Set Variable=VivadoProjectSrcs Value=D:/CASE_STUDY_AES/Vivado/SHIFT_ROWS_LsMLsM/SHIFT_ROWS/SHIFT_ROWS.srcs/sources_1/new;

#-----------------------------------------------------------------------------------------
# Load device
#-----------------------------------------------------------------------------------------

OpenBinFPGA FileName=%ZedBoard%;

#-----------------------------------------------------------------------------------------
# Floorplan the module area  
#-----------------------------------------------------------------------------------------

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y99 LowerRightTile=INT_R_X41Y50;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ModuleArea;

#-----------------------------------------------------------------------------------------
# Generate interface constraints
#-----------------------------------------------------------------------------------------

# Slot (0,0); Border - West
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y77 LowerRightTile=INT_R_X41Y72;
StoreCurrentSelectionAs UserSelectionType=X0Y0_WestBorder;
PrintInterfaceConstraintsForSelection 
  FileName=%VivadoProjectTop%/module_interface_constraints.tcl 
  Append=False
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveWestInput
  Border=West
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=In:2-4:s2p_w;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_w
  NumberOfPrimitives=128;
  
AnnotateSignalNamesToConnectionPrimitives 
  InstantiationFilter=inst_x0y0_w.*
  InputMappingKind=internal
  OutputMappingKind=external
  SignalPrefix=x0y0
  InputSignalName=1
  OutputSignalName=s2p_w
  LookupTableInputPort=3;

# Slot (0,0); Border - North  
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y99 LowerRightTile=INT_R_X41Y94;
StoreCurrentSelectionAs UserSelectionType=X0Y0_NorthBorder;
PrintInterfaceConstraintsForSelection 
  FileName=%VivadoProjectTop%/module_interface_constraints.tcl 
  Append=True
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveNorthOutput
  Border=North
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=Out:2-6:p2s_n;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_n
  NumberOfPrimitives=128;
  
AnnotateSignalNamesToConnectionPrimitives 
  InstantiationFilter=inst_x0y0_n.*
  InputMappingKind=external
  OutputMappingKind=internal
  SignalPrefix=x0y0
  InputSignalName=p2s_n
  OutputSignalName=dummy_n
  LookupTableInputPort=3;
  
# Slot (0,0); Border - East  
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y77 LowerRightTile=INT_R_X41Y72;
StoreCurrentSelectionAs UserSelectionType=X0Y0_EastBorder;
PrintInterfaceConstraintsForSelection 
  FileName=%VivadoProjectTop%/module_interface_constraints.tcl 
  Append=True
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveEastOutput
  Border=East
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=Out:2-4:p2s_e;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_e
  NumberOfPrimitives=128;
  
AnnotateSignalNamesToConnectionPrimitives 
  InstantiationFilter=inst_x0y0_e.*
  InputMappingKind=external
  OutputMappingKind=internal
  SignalPrefix=x0y0
  InputSignalName=p2s_e
  OutputSignalName=dummy_e
  LookupTableInputPort=3; 

# Slot (0,0); Border - South  
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y55 LowerRightTile=INT_R_X41Y50;
StoreCurrentSelectionAs UserSelectionType=X0Y0_SouthBorder;
PrintInterfaceConstraintsForSelection 
  FileName=%VivadoProjectTop%/module_interface_constraints.tcl 
  Append=True
  CreateBackupFile=False
  SignalPrefix=x0y0
  InstanceName=inst_ConnectionPrimitiveSouthOutput
  Border=South
  NumberOfSignals=128
  PreventWiresFromBlocking=True
  InterfaceSpecs=Out:2-6:p2s_s;

InstantiateConnectionPrimitives
  LibraryElementName=LUTConnectionPrimitive
  InstanceName=inst_x0y0_s
  NumberOfPrimitives=128;
  
AnnotateSignalNamesToConnectionPrimitives 
  InstantiationFilter=inst_x0y0_s.*
  InputMappingKind=external
  OutputMappingKind=internal
  SignalPrefix=x0y0
  InputSignalName=p2s_s
  OutputSignalName=dummy_s
  LookupTableInputPort=3;   
  
#-----------------------------------------------------------------------------------------
# Generate connection primitives in VHDL format
#-----------------------------------------------------------------------------------------

# connection primitives that are connected to the input signals of the module
PrintVHDLWrapper 
  InstantiationFilter=inst_x0y0_w.* 
  EntityName=ConnectionPrimitiveWestInput 
  FileName=%VivadoProjectSrcs%/ConnectionPrimitive.vhd 
  Append=False;  
  
PrintVHDLWrapperInstantiation 
  InstantiationFilter=inst_x0y0_w.* 
  EntityName=ConnectionPrimitiveWestInput 
  FileName=%VivadoProjectTop%/ConnectionPrimitive_inst.vhd 
  Append=False;

# connection primitives that are connected to the output signals of the module
PrintVHDLWrapper 
  InstantiationFilter=inst_x0y0_n.* 
  EntityName=ConnectionPrimitiveNorthOutput
  FileName=%VivadoProjectSrcs%/ConnectionPrimitive.vhd 
  Append=True;  
  
PrintVHDLWrapperInstantiation 
  InstantiationFilter=inst_x0y0_n.* 
  EntityName=ConnectionPrimitiveNorthOutput
  FileName=%VivadoProjectTop%/ConnectionPrimitive_inst.vhd 
  Append=True;
  
# connection primitives that are connected to the output signals of the module
PrintVHDLWrapper 
  InstantiationFilter=inst_x0y0_e.* 
  EntityName=ConnectionPrimitiveEastOutput
  FileName=%VivadoProjectSrcs%/ConnectionPrimitive.vhd 
  Append=True;  
  
PrintVHDLWrapperInstantiation 
  InstantiationFilter=inst_x0y0_e.* 
  EntityName=ConnectionPrimitiveEastOutput
  FileName=%VivadoProjectTop%/ConnectionPrimitive_inst.vhd 
  Append=True;

# connection primitives that are connected to the output signals of the module
PrintVHDLWrapper 
  InstantiationFilter=inst_x0y0_s.* 
  EntityName=ConnectionPrimitiveSouthOutput
  FileName=%VivadoProjectSrcs%/ConnectionPrimitive.vhd 
  Append=True;  
  
PrintVHDLWrapperInstantiation 
  InstantiationFilter=inst_x0y0_s.* 
  EntityName=ConnectionPrimitiveSouthOutput
  FileName=%VivadoProjectTop%/ConnectionPrimitive_inst.vhd 
  Append=True;

#-----------------------------------------------------------------------------------------
# Tunnels
#-----------------------------------------------------------------------------------------

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y77 LowerRightTile=INT_R_X37Y72;
StoreCurrentSelectionAs UserSelectionType=InputWestTunnel;
DoNotBlockDoubleEast;
DoNotBlockQuadEast;

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y104 LowerRightTile=INT_R_X41Y100;
StoreCurrentSelectionAs UserSelectionType=OutputNorthTunnel;
DoNotBlockDoubleNorth;
DoNotBlockSexNorth;

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X42Y77 LowerRightTile=INT_R_X51Y72;
StoreCurrentSelectionAs UserSelectionType=OutputEastTunnel;
DoNotBlockDoubleEast;
DoNotBlockQuadEast;

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y49 LowerRightTile=INT_R_X41Y45;
StoreCurrentSelectionAs UserSelectionType=OutputSouthTunnel;
DoNotBlockDoubleSouth;
DoNotBlockSexSouth;

#-----------------------------------------------------------------------------------------
# Generate blocker macro
#-----------------------------------------------------------------------------------------

ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y104 LowerRightTile=INT_R_X51Y45;
SelectUserSelection UserSelectionType=ModuleArea;
StoreCurrentSelectionAs UserSelectionType=BlockerArea;
  
BlockSelection 
  PrintUnblockedPorts=False 
  Prefix=blocker_net 
  BlockWithEndPips=True 
  NetlistContainerName=default_netlist_container;

SaveAsBlocker 
  NetlistContainerNames=default_netlist_container 
  FileName=%VivadoProjectTop%/module_blocker.tcl;

#-----------------------------------------------------------------------------------------
# Generate placement constraints
#-----------------------------------------------------------------------------------------

# module area  
ClearSelection;
SelectUserSelection UserSelectionType=ModuleArea;

PrintAreaConstraint
  InstanceName=inst_Module
  AddResources=True
  FileName=%VivadoProjectTop%/module_placement_constraints.tcl 
  Append=False 
  CreateBackupFile=True;

PrintExcludePlacementProperty
  InstanceName=inst_Module
  FileName=%VivadoProjectTop%/module_placement_constraints.tcl 
  Append=True 
  CreateBackupFile=True;

# connection primitives connected to the input interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X26Y84 LowerRightTile=INT_L_X28Y65;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveWestInput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveWestInput
  AddResources=True
  FileName=%VivadoProjectTop%/module_placement_constraints.tcl 
  Append=True 
  CreateBackupFile=True;

# connection primitives connected to the output interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y144 LowerRightTile=INT_R_X41Y125;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveNorthOutput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveNorthOutput
  AddResources=True
  FileName=%VivadoProjectTop%/module_placement_constraints.tcl 
  Append=True 
  CreateBackupFile=True;
  
# connection primitives connected to the output interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X60Y84 LowerRightTile=INT_L_X62Y65;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveEastOutput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveEastOutput
  AddResources=True
  FileName=%VivadoProjectTop%/module_placement_constraints.tcl 
  Append=True 
  CreateBackupFile=True;
  
# connection primitives connected to the output interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X38Y34 LowerRightTile=INT_R_X41Y15;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveSouthOutput;

PrintAreaConstraint
  InstanceName=inst_ConnectionPrimitiveSouthOutput
  AddResources=True
  FileName=%VivadoProjectTop%/module_placement_constraints.tcl 
  Append=True 
  CreateBackupFile=True;


